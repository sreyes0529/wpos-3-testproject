package com.stratpoint.stratpointpos;

import android.app.Application;

import cn.weipass.pos.sdk.Weipos;
import cn.weipass.pos.sdk.impl.WeiposImpl;

/**
 * Created by angelaalmoro on 3/28/17.
 */

public class StratpointPosApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        WeiposImpl.as().init(this, new Weipos.OnInitListener() {
            @Override
            public void onInitOk() {

            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onDestroy() {

            }
        });
    }

}
