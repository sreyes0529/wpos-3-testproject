package com.stratpoint.spandroidframework.conn;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.stratpoint.spandroidframework.conn.controller.GeneralPreferenceController;
import com.stratpoint.spandroidframework.model.BaseResponse;
import com.stratpoint.spandroidframework.utils.AppConstants;
import com.stratpoint.spandroidframework.utils.FCfg;
import com.stratpoint.spandroidframework.utils.NetworkUtils;
import com.stratpoint.spandroidframework.R;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;


public class HttpConnection implements AppConstants {
    private Context mContext;
    private HttpConnectionResponse mHttpConnectionResponse;
    private HttpConnectionInputStreamResponse mHttpConnectionInputStreamResponse;
    private ErrorResponseCallback.OnErrorResponse mErrorResponse;
    public static final String HTTP_RESPONSE_RETRY = "HTTP_RESPONSE_RETRY";
    public static final String HTTP_RESPONSE_NO_INTERNET = "HTTP_RESPONSE_NO_INTERNET";
    public static final String HTTP_RESPONSE_UNKNOWN = "HTTP_RESPONSE_UNKNOWN";
    public static final String HTTP_RESPONSE_OK = "200";
    public static final String HTTP_RESPONSE_400 = "400";
    public static final String HTTP_RESPONSE_401 = "401";
    public static final String HTTP_RESPONSE_404 = "404";
    public static final String HTTP_RESPONSE_500 = "500";
    public static final String HTTP_RESPONSE_503 = "503";

    private Call mCall;
    protected SharedPreferences generalPreferences;
    private GeneralPreferenceController genPrefController;
    private final String TAG = "HTTPConnection.class";

    public HttpConnection() {
    }

    public HttpConnection(Context context, HttpConnectionResponse httpConnectionResponse, ErrorResponseCallback.OnErrorResponse errorResponse) {
        mContext = context;
        mHttpConnectionResponse = httpConnectionResponse;
        mErrorResponse = errorResponse;
        generalPreferences = mContext.getSharedPreferences(PrefKeys.GEN_PREF, Context.MODE_PRIVATE);
        genPrefController = new GeneralPreferenceController(mContext, generalPreferences);
    }

    public HttpConnection(Context context, HttpConnectionInputStreamResponse httpConnectionInputStreamResponse, ErrorResponseCallback.OnErrorResponse errorResponse) {
        mContext = context;
        mHttpConnectionInputStreamResponse = httpConnectionInputStreamResponse;
        mErrorResponse = errorResponse;
        generalPreferences = mContext.getSharedPreferences(PrefKeys.GEN_PREF, Context.MODE_PRIVATE);
        genPrefController = new GeneralPreferenceController(mContext, generalPreferences);
    }


    /**
     * GET Request for api without parameters only.
     */
    public void GET(String url) {
        try {
            GET(mContext, url, null);
        } catch (IOException e) {
            Log.e("HTTPConnection: " + url, e.getMessage());
        }
    }

    /**
     * GET Request from third party api.
     */
    public void GET_EXTERNAL(String url) {
        if (!NetworkUtils.isNetworkConnectedOrConnecting(mContext)) {
            noInternetConnection();
            return;
        }

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(url);

        Request request = requestBuilder.build();

        OkHttpClient client = newOkHttpClient();

        initiateRequest(request, client);
    }

    /**
     * GET Request from third party api with parameter.
     */
    public void GET_EXTERNAL(String url, List<GETParameter> getParameters) {
        if (!NetworkUtils.isNetworkConnectedOrConnecting(mContext)) {
            noInternetConnection();
            return;
        }

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(buildExternalAPIGETURL(url, getParameters));

        Request request = requestBuilder.build();

        OkHttpClient client = newOkHttpClient();

        initiateRequest(request, client);
    }

    /**
     * GET Request for api with parameters.
     *
     * @param getParameters - List Of GETParameter
     */
    public void GET(String url, List<GETParameter> getParameters) {
        try {
            GET(mContext, url, getParameters);
        } catch (IOException e) {
            Log.e("HTTPConnection: " + url, e.getMessage());
        }
    }

    /**
     * GET Request for api with parameters.
     */
    private void GET(Context context, String url, List<GETParameter> getParameters) throws IOException {

        if (!NetworkUtils.isNetworkConnectedOrConnecting(context)) {
            noInternetConnection();
            return;
        }

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(buildAPIGETURL(url, getParameters));
        //TODO: add header if any
//        requestBuilder.addHeader("Authorization", "Bearer ");

        Request request = requestBuilder.build();

        OkHttpClient client = newOkHttpClient();

        initiateRequest(request, client);

    }

    /**
     * URL Builder for external GET
     *
     * @param url - api path
     * @return API URL
     */
    private HttpUrl buildExternalAPIGETURL(String url, List<GETParameter> getParameters) {
        StringBuffer urlStringBuffer = new StringBuffer();
        urlStringBuffer.append(url);

        return buildGETURL(url.toString(), getParameters);
    }


    /**
     * URL Builder for GET
     *
     * @param apiPath - api path
     * @return API URL
     */
    private HttpUrl buildAPIGETURL(String apiPath, List<GETParameter> getParameters) {
        String apiUrL = buildAPIURL(apiPath);
        return  buildGETURL(apiUrL, getParameters);

    }

    /**
     *
     * @param -
     * */
    private HttpUrl buildGETURL(String apiURL, List<GETParameter> getParameters) {
        HttpUrl httpUrl = HttpUrl.parse(apiURL);
        HttpUrl.Builder httpBuilder = httpUrl.newBuilder();
        if (null != getParameters) {
            for (GETParameter getParameter : getParameters) {
                try {
                    httpBuilder.addEncodedQueryParameter(getParameter.getKey(), URLEncoder.encode(getParameter.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    Log.e("URlEncodingException", e.toString());
                }
            }
        }


        return httpBuilder.build();
    }

    /**
     * @param apiPath - api path
     * @return API URL
     */
    private String buildAPIURL(String apiPath) {
        StringBuffer urlStringBuffer = new StringBuffer();
        urlStringBuffer.append(FCfg.Networks.SERVER_PROTOCOL);
        urlStringBuffer.append(FCfg.Networks.SERVER_DOMAIN);

        urlStringBuffer.append(apiPath);

        return urlStringBuffer.toString();
    }


    /**
     * POST Method with formbody,
     * use FormEncodingBuilder
     * <p/>
     * <p/>
     * example:
     * formBody = new FormBody.Builder();
     * formBody.add("search", "Jurassic Park");
     */
    public void POST(String apiPath, FormBody.Builder formEncodingBuilder) {
        if (!NetworkUtils.isNetworkConnectedOrConnecting(mContext)) {
            noInternetConnection();
            return;
        }
        RequestBody formBody = formEncodingBuilder.build();

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(buildAPIURL(apiPath));
        //TODO: Add header if any
//        requestBuilder.addHeader("Authorization", "Bearer " +"");
        requestBuilder.post(formBody);

        Request request = requestBuilder.build();

        OkHttpClient client = newOkHttpClient();

        initiateRequest(request, client);
    }

    private void initiateRequest(final Request request, final OkHttpClient client) {
        mCall = client.newCall(request);
        mCall.enqueue(new Callback() {

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    Gson gson = new Gson();
                    if (response.isSuccessful()) {
                        if (mHttpConnectionInputStreamResponse != null) {
                            mHttpConnectionInputStreamResponse.onResponse(response.body());
                        }
                        if (mHttpConnectionResponse != null) {
                            mHttpConnectionResponse.onResponse(response.body().string());
                        }
                    } else if (response.code() == 400) {
                        mHttpConnectionResponse.onResponse(response.body().string());
                    } else if (response.code() == 404) {
                        mHttpConnectionResponse.onResponse(response.body().string());
                    } else if (response.code() == 401) {
                        BaseResponse baseResponse = new BaseResponse();
                        baseResponse.setStatusCode(HTTP_RESPONSE_401);
                        mHttpConnectionResponse.onResponse(gson.toJson(baseResponse));
                    } else if (response.code() == 500) {
                        mHttpConnectionResponse.onResponse(response.body().string());
                    } else {
                        BaseResponse baseResponse = new BaseResponse();
                        baseResponse.setTitle(mContext.getString(R.string.error_title));
                        baseResponse.setMessage(mContext.getString(R.string.error_message));
                        baseResponse.setStatusCode(HTTP_RESPONSE_UNKNOWN);
                        mHttpConnectionResponse.onResponse(gson.toJson(baseResponse));
                    }
                } catch (IOException e) {
                    Log.e("HTTPConnection: ", e.getMessage());
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setTitle(mContext.getString(R.string.error_title));
                    baseResponse.setMessage(mContext.getString(R.string.error_message));
                    baseResponse.setStatusCode(HTTP_RESPONSE_UNKNOWN);
                    mErrorResponse.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call call, IOException arg1) {
                mHttpConnectionResponse.onFailure(call, arg1);
                initRetry();
            }
        });
    }


    /**
     * PUT Method with formbody,
     * use FormEncodingBuilder
     * <p/>
     * example:
     * formBody = new FormBody.Builder();
     * formBody.add("search", "Jurassic Park");
     */
    public void PUT(String apiPath, FormBody.Builder formEndcodingBuilder) {

        RequestBody formBody = formEndcodingBuilder.build();

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(buildAPIURL(apiPath));
        //TODO: add header if any
//        requestBuilder.addHeader("Authorization", "Bearer ");
        requestBuilder.put(formBody);

        Request request = requestBuilder.build();
        OkHttpClient client = newOkHttpClient();

        initiateRequest(request, client);


    }

    /**
     * DELETE Method with access token header and formbody,
     * use FormEncodingBuilder
     * <p/>
     * example:
     * formBody = new FormBody.Builder();
     * formBody.add("search", "Jurassic Park");
     */
    public void DELETE(String apiPath, FormBody.Builder formEndcodingBuilder) {

        RequestBody formBody = formEndcodingBuilder.build();
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(buildAPIURL(apiPath));
        //TODO: addheader if any
//        requestBuilder.addHeader("Authorization", "Bearer ");
        requestBuilder.delete(formBody);


        Request request = requestBuilder.build();
        OkHttpClient client = newOkHttpClient();

        initiateRequest(request, client);
    }

    /**
     * DELETE Method with access token header
     */
    public void DELETE(String apiPath) {

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(buildAPIURL(apiPath));
        //TODO: add header if any
//        requestBuilder.addHeader("Authorization", "Bearer ");
        requestBuilder.delete();

        Request request = requestBuilder.build();
        OkHttpClient client = newOkHttpClient();

        initiateRequest(request, client);
    }

    private OkHttpClient newOkHttpClient() {
        return newSSLOkHttpClient(CONNECTION_TIMEOUT, WRITE_TIMEOUT, READ_TIMEOUT);
    }

    /**
     * override http calls for these timeouts
     * */
    private OkHttpClient newOkHttpClient(long connectionTimeout, long writeTimeout, long readTimeout) {
        return newSSLOkHttpClient(connectionTimeout, writeTimeout, readTimeout);
    }

    /**
     * Get OkHttpClient which ignores all SSL errors
     */
    private OkHttpClient newSSLOkHttpClient(long connectionTimeout, long writeTimeout, long readTimeout) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(connectionTimeout, TimeUnit.SECONDS)
                    .writeTimeout(writeTimeout, TimeUnit.SECONDS)
                    .readTimeout(readTimeout, TimeUnit.SECONDS)
                    .sslSocketFactory(sslSocketFactory)
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    }).build();

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    private void noInternetConnection() {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setTitle(mContext.getString(R.string.network_error_title));
        baseResponse.setMessage(mContext.getString(R.string.network_error_message));
        baseResponse.setStatusCode(HTTP_RESPONSE_NO_INTERNET);
        mErrorResponse.onError(baseResponse);
    }

    private void initRetry() {
        Gson gson = new Gson();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setStatusCode(HTTP_RESPONSE_RETRY);
        mHttpConnectionResponse.onResponse(gson.toJson(baseResponse));
    }


    public void cancelRequest() {
        if (null != mCall) {
            mCall.cancel();
        }
    }


    /**
     * Interface to implements on classes that needs HTTConnectionResponse
     */
    public interface HttpConnectionResponse {
        void onResponse(String response);

        void onFailure(Call call, IOException arg1);
    }

    public interface HttpConnectionSetter {
        void onSetHttpConnection(HttpConnection httpConnection);
    }

    public interface HttpConnectionInputStreamResponse {
        void onResponse(ResponseBody responseBody);

        void onFailure(Call call, IOException arg1);
    }
}
