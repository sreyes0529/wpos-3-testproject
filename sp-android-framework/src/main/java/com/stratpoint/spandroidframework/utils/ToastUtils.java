package com.stratpoint.spandroidframework.utils;

import android.app.Activity;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Created by adelumban on 8/27/15.
 */
public class ToastUtils {

    public static void toastTOP(Activity activity, String text, int toastLength) {
        Toast toast = initToast(activity, text, toastLength, Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        showToast(activity, toast);
    }


    public static void toastBOTTOM(Activity activity, String text, int toastLength) {
        Toast toast = initToast(activity, text, toastLength, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        showToast(activity, toast);
    }

    private static void showToast(Activity activity, final Toast toast) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                toast.show();
            }
        });
    }

    /**
     * @param gravity - set the position of the toast.
     */
    private static Toast initToast(Activity activity, String text, int toastLength, int gravity, int xOffset, int yOffset) {

        Toast toast = Toast.makeText(activity, text, toastLength);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        return toast;
    }
}
