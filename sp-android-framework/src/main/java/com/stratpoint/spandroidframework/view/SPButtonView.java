package com.stratpoint.spandroidframework.view;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.stratpoint.spandroidframework.R;


public class SPButtonView extends Button {

    @SuppressLint("NewApi")
    public SPButtonView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if (!isInEditMode()) {
            init(attrs);
        }
    }

    public SPButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            init(attrs);
        }
    }

    public SPButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init(attrs);
        }
    }

    public SPButtonView(Context context) {
        super(context);
        if (!isInEditMode()) {
            init(null);
        }
    }

    private void init(AttributeSet attrs) {
        setTransformationMethod(null);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.fontface);
            String fontpath = a.getString(R.styleable.fontface_fontpath);
            if (fontpath != null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), fontpath);
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }

}
