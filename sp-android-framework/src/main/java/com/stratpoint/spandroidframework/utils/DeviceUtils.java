package com.stratpoint.spandroidframework.utils;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.DisplayMetrics;

/**
 * Created by adelumban on 8/19/15.
 */
public class DeviceUtils {

    public static int getCurrentOsVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    /**
     * Code used from:
     * https://gist.github.com/abombss/844325
     */
    public static String getUserAgent(Context context) {
        String userAgent = "sp-android-app";
        try {
            String appName = "";
            String appVersion = "";
            Application app = (Application) context.getApplicationContext();
            DisplayMetrics display = app.getResources().getDisplayMetrics();
            Configuration config = app.getResources().getConfiguration();

            // Always send screen dimension for portrait mode
            int height = (config.orientation == Configuration.ORIENTATION_LANDSCAPE) ? display.widthPixels : display.heightPixels;
            int width = (config.orientation == Configuration.ORIENTATION_LANDSCAPE) ? display.heightPixels : display.widthPixels;

            try {
                PackageInfo packageInfo = app.getPackageManager().getPackageInfo(app.getPackageName(), PackageManager.GET_CONFIGURATIONS);
                appName = packageInfo.packageName;
                appVersion = packageInfo.versionName;
            } catch (PackageManager.NameNotFoundException ignore) {
                // this should never happen, we are looking up our self
            }

            // Tries to conform to default android UA string without the Safari / webkit noise, plus adds the screen dimensions
            userAgent = String.format("%1$s/%2$s (%3$s; U; Android %4$s; %5$s-%6$s; %12$s Build/%7$s; %8$s) %9$dX%10$d %11$s %12$s"
                    , appName
                    , appVersion
                    , System.getProperty("os.name", "Linux")
                    , Build.VERSION.RELEASE
                    , config.locale.getLanguage().toLowerCase()
                    , config.locale.getCountry().toLowerCase()
                    , Build.ID
                    , Build.BRAND
                    , width
                    , height
                    , Build.MANUFACTURER
                    , Build.MODEL);
        } catch (Exception e) {
        }
        return userAgent;
    }


    public static boolean isAppInstalled(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    /**
     *@param  packageName - packagename of application checkout playstore.google.com and get the id in url
     * */
    public static void startInstalledApp(Context context, String packageName) {
        if (isAppInstalled(context, packageName)) {
            context.startActivity(context.getPackageManager().getLaunchIntentForPackage(packageName));
        } else {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("market://details?id=" + packageName));

            context.startActivity(i);
        }
    }
}
