package com.stratpoint.spandroidframework.conn;

/**
 * Created by adelumban on 8/18/15.
 */
public class GETParameter {
    private String key;
    private String value;
    public GETParameter(String key, String value){
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
