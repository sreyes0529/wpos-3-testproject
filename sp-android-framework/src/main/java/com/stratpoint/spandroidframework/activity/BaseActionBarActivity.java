package com.stratpoint.spandroidframework.activity;


import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

import com.google.gson.Gson;
import com.stratpoint.spandroidframework.conn.ErrorResponseCallback;
import com.stratpoint.spandroidframework.conn.HttpConnection;
import com.stratpoint.spandroidframework.conn.ServerRequest;
import com.stratpoint.spandroidframework.conn.controller.GeneralPreferenceController;
import com.stratpoint.spandroidframework.fragment.BaseFragment;
import com.stratpoint.spandroidframework.model.BaseResponse;
import com.stratpoint.spandroidframework.utils.AppConstants;
import com.stratpoint.spandroidframework.utils.DialogUtil;
import com.stratpoint.spandroidframework.utils.FCfg;
import com.stratpoint.spandroidframework.utils.NetworkUtils;
import com.stratpoint.spandroidframework.utils.ResponseUtil;
import com.stratpoint.spandroidframework.view.ProgressDialog;
import com.stratpoint.spandroidframework.R;

import java.io.IOException;

import butterknife.ButterKnife;
import okhttp3.Call;

public class BaseActionBarActivity extends AppCompatActivity implements HttpConnection.HttpConnectionResponse,
        ErrorResponseCallback.OnErrorResponse, HttpConnection.HttpConnectionSetter, ServerRequest, AppConstants {
    protected Context mContext;
    protected HttpConnection mHttpConnection;
    protected SharedPreferences generalPreferences;
    protected GeneralPreferenceController genPrefController;
    protected Fragment currentFragment;
    protected int httpRequestID = -1;
    private String TAG_PROGRESS = "progress_tag";
    private ProgressDialog progressDialog;
    protected Gson mGson = new Gson();
    private ServerRequest mServerRequest;


    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
//        if (Build.VERSION.SDK_INT > 20) {
//            overridePendingTransition(0, android.R.transition.fade);
//        } else {
//            overridePendingTransition(0, android.R.anim.fade_out);
//        }
        init();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        init();
    }

    @Override
    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        init();
    }

    /**
     * called after setcontentView
     **/
    protected void init() {
        generalPreferences = getSharedPreferences(PrefKeys.GEN_PREF, Context.MODE_PRIVATE);
        genPrefController = new GeneralPreferenceController(this, generalPreferences);
        ButterKnife.bind(this);
        mContext = this;
        mHttpConnection = new HttpConnection(this, this, this);
        progressDialog = new ProgressDialog();

        setmServerRequest(this);
    }

    public void setmServerRequest(ServerRequest mServerRequest) {
        this.mServerRequest = mServerRequest;
    }

    /**
     * called after onPostCreate
     **/
    protected void populateViews() {
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        populateViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        endProgress();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (null != progressDialog) {
            if (progressDialog.isShowing()) {
                progressDialog.onPause();
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (null != progressDialog) {
            if (progressDialog.isShowing()) {
                progressDialog.onResume();
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cancelConnection();
    }


    /**
     * show progress dialog on httpConnection
     * use the parameter id to determine the view that has been clicked
     * to handle different request
     *
     * @param id
     * @param hasProgress - set a progress to be visible or not.
     **/
    public void initHttpConnection(int id, boolean hasProgress) {
        httpRequestID = id;
        if (hasProgress) {
            startProgress();
        }
        mServerRequest.onServerRequest(id);
    }

    /**
     * show progress dialog on httpConnection
     * use the parameter id to determine the view that has been clicked
     * to handle different request
     *
     * @param id
     **/
    public void initHttpConnection(int id) {
        initHttpConnection(id, true);
    }

    @Override
    public void onServerRequest(int id) {

    }


    public void startProgress() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!progressDialog.isShowing()) {
                    progressDialog.show(getFragmentManager(), TAG_PROGRESS);
                }
            }
        });
    }

    public void initHttpConnectionFail() {
        endProgress();
    }

    public void endProgress() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (null != progressDialog) {
                    if (progressDialog.isShowing()) {
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.remove(progressDialog).commitAllowingStateLoss();
                        progressDialog.setIsShowing(false);
                    }
                }

            }
        });

    }


    public void cancelConnection() {
        if (null != mHttpConnection) {
            mHttpConnection.cancelRequest();
        }
    }

    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onFailure(Call call, IOException arg1) {

    }


    @Override
    public void onError(final BaseResponse baseResponse) {
        endProgress();
        initResponseHandler(baseResponse);
    }

    @Override
    public void finish() {
        super.finish();
//        if (Build.VERSION.SDK_INT > 20) {
//            overridePendingTransition(0, android.R.transition.fade);
//        } else {
//            overridePendingTransition(0, android.R.anim.fade_out);
//        }

    }

    public void setProgressCancelListner(final ProgressDialog.CancelListner cancelListner) {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.setCancelListener(cancelListner);
            }
        }, 1000);
    }

    public void setProgressDialogMessage(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.setMessage(message);
            }
        });
    }

    public void showOKDialog(final Context context, final String title, final String message, final String positiveTxt, final DialogUtil.DialogOnClickListener positiveDListener) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtil.showOK(context, title, message, positiveTxt, positiveDListener);
            }
        });
    }

    public void showValidationOKDialog(final Context context, final String title, final String message, final String positiveTxt, final DialogUtil.DialogOnClickListener positiveDListener) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtil.showColoredOK(context, title, message, positiveTxt, positiveDListener);
            }
        });
    }

    public void showValidationOKCancelDialog(final Context context, final String title, final String message, final String positiveTxt, final DialogUtil.DialogOnClickListener positiveDListener, final String negativeTxt, final DialogUtil.DialogOnClickListener negativeDListener) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtil.showColoredOKCancel(context, title, message, positiveTxt, positiveDListener, negativeTxt, negativeDListener);
            }
        });
    }


    public void showOKCancelDialog(final Context context, final String title, final String message, final String positiveTxt, final DialogUtil.DialogOnClickListener positiveDListener, final String negativeTxt, final DialogUtil.DialogOnClickListener negativeDListener) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtil.showOKCancel(context, title, message, positiveTxt, positiveDListener, negativeTxt, negativeDListener);
            }
        });

    }

    public void onResponse401() {
        showOKDialog(this, getString(R.string.unauthorized_title), getString(R.string.unauthorized_message), getString(R.string.ok), new DialogUtil.DialogOnClickListener() {
            @Override
            public void onClick() {
                onLogout();
            }
        });
    }

    protected void onLogout() {
        clearPreference();
    }

    protected void clearPreference() {
        genPrefController.clear();
    }


    @Override
    public void onSetHttpConnection(HttpConnection httpConnection) {
        mHttpConnection = httpConnection;
    }

    public void initResponseHandler(BaseResponse baseResponse) {
        if (isFinishing()) {
            return;
        }
        if (ResponseUtil.isResponseHeader401(baseResponse)) {
            onResponse401();
        } else if (ResponseUtil.isResponseHeader400(baseResponse) || ResponseUtil.isResponseHeader500(baseResponse)) {
            showValidationOKDialog(this, baseResponse.getTitle(), baseResponse.getMessage(), getString(R.string.ok), null);
        } else if (ResponseUtil.isResponseRetry(baseResponse)) {
            showValidationOKCancelDialog(this, getString(R.string.network_error_title), getString(R.string.network_error_message),
                    getString(R.string.retry), new DialogUtil.DialogOnClickListener() {
                        @Override
                        public void onClick() {
                            onRefresh();
                        }
                    }, getString(R.string.cancel), null);
        } else if (ResponseUtil.isResponseNoNetwork(baseResponse)) {
            noInternetConnection();
        } else if (ResponseUtil.isResponseHeader500(baseResponse)) {
            showValidationOKDialog(this, baseResponse.getTitle(), baseResponse.getMessage(), getString(R.string.ok), null);
        } else {
            showValidationOKDialog(this, getString(R.string.error_title), getString(R.string.error_message), getString(R.string.ok), null);
        }
    }

    private void onRefresh() {
        if (null != currentFragment) {
            if (currentFragment instanceof BaseFragment) {
                ((BaseFragment) currentFragment).refreshLastRequest();
            }
        } else {
            initHttpConnection(httpRequestID);
        }
    }

    /**
     * initialize custom action bar yourself
     * the config below hides the tile
     * left icon triggers prev page
     * customEnabled
     * use your custom view for action bar
     **/
    public void initActionBar() {
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowTitleEnabled(false);
        actionbar.setDisplayShowCustomEnabled(true);
        //actionbar.setCustomView(headerView);
    }

    public void minimizeApp() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
    }


    public boolean hasInternetConnection() {
        if (!NetworkUtils.isNetworkConnectedOrConnecting(mContext)) {
            noInternetConnection();
            return false;
        }
        return true;
    }

    protected void noInternetConnection() {
        showValidationOKDialog(this, getString(R.string.network_error_title), getString(R.string.network_error_title), getString(R.string.ok), null);
    }


    public void configServer() {
        genPrefController.saveProtocol(FCfg.Networks.SERVER_PROTOCOL);
        genPrefController.saveDomain(FCfg.Networks.SERVER_DOMAIN);
    }

}
