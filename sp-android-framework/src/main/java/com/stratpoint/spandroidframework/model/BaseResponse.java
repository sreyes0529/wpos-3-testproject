package com.stratpoint.spandroidframework.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by adelumban on 4/29/16.
 */
public class BaseResponse {

    @SerializedName("status_code")
    private String statusCode;

    private String title;

    private String message;

    @SerializedName("response_date")
    private long responseDate;

    public long getResponseDate() {
        return responseDate * 1000l;
    }

    public void setResponseDate(String response_date) {
        this.responseDate = responseDate;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
