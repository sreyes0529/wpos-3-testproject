package com.stratpoint.stratpointpos.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.Toast;

import com.stratpoint.spandroidframework.activity.BaseActionBarActivity;
import com.stratpoint.spandroidframework.view.SPButtonView;
import com.stratpoint.spandroidframework.view.SPTextView;
import com.stratpoint.stratpointpos.R;

import butterknife.BindView;
import cn.weipass.pos.sdk.IPrint;
import cn.weipass.pos.sdk.MagneticReader;
import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;

/**
 * Created by angelaalmoro on 3/28/17.
 */

public class ReadCardActivity extends BaseActionBarActivity {
    SPTextView mCardContentTv;

    private MagneticReader mMagneticReader;
    private Printer mPrinter;
    private ReadMagTask mReadMagTask;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_card);
    }

    @Override
    protected void init() {
        super.init();

        mReadMagTask = null;
        mCardContentTv = (SPTextView) findViewById(R.id.card_content_tv);

        mMagneticReader = WeiposImpl.as().openMagneticReader();
        if (mMagneticReader == null) {
            Toast.makeText(this, getResources().getString(R.string.magnetic_reader_not_available), Toast.LENGTH_SHORT).show();
            finish();
        }

        mPrinter = WeiposImpl.as().openPrinter();
        if (mPrinter == null) {
            Toast.makeText(this, getResources().getString(R.string.printer_not_available), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTask();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopTask();
    }

    private void startTask(){
        if(mReadMagTask == null){
            mReadMagTask = new ReadMagTask();
            mReadMagTask.start();
        }
    }

    private void stopTask(){
        if(mReadMagTask != null){
            mReadMagTask.interrupt();
            mReadMagTask = null;
        }
    }

    class ReadMagTask extends Thread implements Handler.Callback {
        private Handler H;
        private boolean isRun = false;

        public ReadMagTask() {
            H = new Handler(this);
        }

        /*
         * (non-Javadoc)
         *
         * @see java.lang.Thread#run()
         */
        @Override
        public void run() {
            isRun = true;
            // 磁卡刷卡后，主动获取解码后的字符串数据信息
            try {
                while (isRun) {
                    String decodeData = getMagneticReaderInfo();
                    if (decodeData != null && decodeData.length() != 0) {
                        System.out.println("final============>>>"+decodeData);
                        Message m = H.obtainMessage(0);
                        m.obj = decodeData;
                        H.sendMessage(m);
                    }

                    Thread.sleep(500);
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                isRun = false;
            }
        }

        @Override
        public boolean handleMessage(Message msg) {
            updateLogInfo(getResources().getString(R.string.magnetic_card_content) + msg.obj);

            mPrinter.printText("Welcome to Stratpoint POS\n\n", Printer.FontFamily.SONG, Printer.FontSize.MEDIUM, Printer.FontStyle.NORMAL, IPrint.Gravity.CENTER);
            mPrinter.printText(msg.obj + "\n\n\n\n\n\n\n\n", Printer.FontFamily.SONG, Printer.FontSize.SMALL, Printer.FontStyle.NORMAL, IPrint.Gravity.CENTER);
            return false;
        }

    }

    private void updateLogInfo(String msg) {
        if(mCardContentTv.getLineCount()>=30){
            setTestRange();
        }
        mCardContentTv.append("\n" + msg + "\n\n");
    }

    private void setTestRange() {
        StringBuilder sb = new StringBuilder();
        sb.append(getResources().getString(R.string.read_magnetic_card_info) + ": ");

        mCardContentTv.setText(sb.toString());
    }

    public String getMagneticReaderInfo() {
        if (mMagneticReader == null) {
            return "";
        }

        String[] decodeData  = mMagneticReader.getCardDecodeThreeTrackData();// getCardDecodeData();
        if (decodeData != null && decodeData.length > 0) {
            String retStr = "";
            for (int i = 0; i < decodeData.length; i++) {
                if (decodeData[i] == null)
                    continue;
                String txt = decodeData[i].trim();
                if (retStr.length() > 0){
                    retStr = retStr + "=";
                }else{
                    if(txt.indexOf("=")>=0){
                        String[] arr = txt.split("=");
                        if(arr[0].length()==16 || arr[0].length()==19){
                            return arr[0];
                        }
                    }
                }
                retStr = retStr + txt;
            }
            return retStr;
        } else {
            return "";
        }
    }
}
