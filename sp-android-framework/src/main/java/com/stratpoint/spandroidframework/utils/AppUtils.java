package com.stratpoint.spandroidframework.utils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import java.util.List;

public class AppUtils {

    public static String getVersionName(Context context) {
        String versionName = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionName = pInfo.versionName;
        } catch (NameNotFoundException e) {
            Log.e("versionName: ", e.getMessage());
        }
        return versionName;
    }

    public static boolean isVersionUpdated(Context context, String newVersion) {
        if (isProdBuild()) {
            return true;
        }
        String appVersion = getVersionName(context);
        String siteVersion = newVersion;

        String[] appVersionSplit = appVersion.split("\\.");
        String[] siteVersionSplit = siteVersion.split("\\.");

        boolean isUpdated = true;

        int siteMajor = Integer.parseInt(siteVersionSplit[0]);
        int siteMinor = Integer.parseInt(siteVersionSplit[1]);
        int siteMaintenance = Integer.parseInt(siteVersionSplit[2]);


        int appMajor = Integer.parseInt(appVersionSplit[0]);
        int appMinor = Integer.parseInt(appVersionSplit[1]);
        int appMaintenance = Integer.parseInt(appVersionSplit[2]);


        if (siteMajor > appMajor) {
            isUpdated = false;
        } else if (appMajor > siteMajor) {
            isUpdated = true;
        } else {
            if (siteMinor > appMinor) {
                isUpdated = false;
            } else if (appMinor > siteMinor) {
                isUpdated = true;
            } else {
                if (siteMaintenance > appMaintenance) {
                    isUpdated = false;
                } else if (appMaintenance >= siteMaintenance) {
                    isUpdated = true;
                }
            }
        }
        return isUpdated;
    }

    public static boolean isInBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        Log.d("current task :", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClass().getSimpleName());
        ComponentName componentInfo = taskInfo.get(0).topActivity;
        if (componentInfo.getPackageName().equalsIgnoreCase(context.getPackageName())) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isProdBuild(){
        return FCfg.isProdBuild;
    }
}
