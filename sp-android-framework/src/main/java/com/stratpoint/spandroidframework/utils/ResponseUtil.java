package com.stratpoint.spandroidframework.utils;


import com.stratpoint.spandroidframework.conn.HttpConnection;
import com.stratpoint.spandroidframework.model.BaseResponse;

/**
 * Created by adelumban on 1/29/16.
 */
public class ResponseUtil {

    public static boolean isResponseHeaderOK(BaseResponse baseResponse) {
        return baseResponse.getStatusCode().equals(HttpConnection.HTTP_RESPONSE_OK);

    }

    public static boolean isResponseHeader400(BaseResponse baseResponse) {
        return baseResponse.getStatusCode().equals(HttpConnection.HTTP_RESPONSE_400);
    }

    public static boolean isResponseHeader401(BaseResponse baseResponse) {
        return baseResponse.getStatusCode().equals(HttpConnection.HTTP_RESPONSE_401);
    }

    public static boolean isResponseHeader404(BaseResponse baseResponse) {
        return baseResponse.getStatusCode().equals(HttpConnection.HTTP_RESPONSE_404);
    }

    public static boolean isResponseHeader500(BaseResponse baseResponse) {
        return baseResponse.getStatusCode().equals(HttpConnection.HTTP_RESPONSE_500);
    }

    public static boolean isResponseHeader503(BaseResponse baseResponse) {
        return baseResponse.getStatusCode().equals(HttpConnection.HTTP_RESPONSE_503);
    }

    public static boolean isResponseRetry(BaseResponse baseResponse) {
        return baseResponse.getStatusCode().equals(HttpConnection.HTTP_RESPONSE_RETRY);
    }

    public static boolean isResponseNoNetwork(BaseResponse baseResponse) {
        return baseResponse.getStatusCode().equals(HttpConnection.HTTP_RESPONSE_NO_INTERNET);
    }

}
