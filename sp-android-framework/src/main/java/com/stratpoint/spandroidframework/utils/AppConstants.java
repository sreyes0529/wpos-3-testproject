package com.stratpoint.spandroidframework.utils;

/**
 * Created by adelumban on 9/19/16.
 * DEBUG
 */
public interface AppConstants {


    String GOOGLE_API_PROJECTID = "";
    String GOOGLE_API_CLIENTID = "blahblahlah.apps.googleusercontent.com";

    interface PrefKeys {
        String PROTOCOL = "protocol_cfg";
        String DOMAIN = "domain_cfg";

        //TODO: set your appname here
        String GEN_PREF = "appname_here" + "_general_prefs";
    }

    int CONNECTION_TIMEOUT = 10;
    int WRITE_TIMEOUT = 10;
    int READ_TIMEOUT = 30;


}
