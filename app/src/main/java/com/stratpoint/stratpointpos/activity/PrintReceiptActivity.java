package com.stratpoint.stratpointpos.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.stratpoint.spandroidframework.activity.BaseActionBarActivity;
import com.stratpoint.spandroidframework.view.SPButtonView;
import com.stratpoint.spandroidframework.view.SPEditText;
import com.stratpoint.stratpointpos.R;

import java.io.ByteArrayOutputStream;

import butterknife.BindView;
import butterknife.OnClick;
import cn.weipass.pos.sdk.IPrint;
import cn.weipass.pos.sdk.LatticePrinter;
import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;

/**
 * Created by angelaalmoro on 3/28/17.
 */

public class PrintReceiptActivity extends BaseActionBarActivity {
    SPEditText mPrintEt;
    @BindView(R.id.submit_btn) SPButtonView mSubmitBtn;

    private LatticePrinter mPrinter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_receipt);
    }

    @Override
    protected void init() {
        super.init();

        mPrintEt = (SPEditText) findViewById(R.id.print_et);

        mPrinter = WeiposImpl.as().openLatticePrinter();
        if (mPrinter == null) {
            Toast.makeText(this, getResources().getString(R.string.printer_not_available), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @OnClick(R.id.submit_btn)
    public void onSubmit(View view) {
        switch (view.getId()) {
            case R.id.submit_btn:
                String message = mPrintEt.getText().toString();
                if (message.length() > 0) {
                    printMessage(message);
                }
                break;
        }
    }

    private void printMessage(String message) {
        Drawable logo = getResources().getDrawable(
                R.drawable.logo);
        mPrinter.printImage(bitmap2Bytes(drawableToBitmap(logo)),
                IPrint.Gravity.CENTER);

        mPrinter.printText("Welcome to Stratpoint POS\n\nThis is your message:\n" + "\n", LatticePrinter.FontFamily.SONG, LatticePrinter.FontSize.MEDIUM,
                LatticePrinter.FontStyle.BOLD);
        mPrinter.printText(message + "\n\n", LatticePrinter.FontFamily.SONG, LatticePrinter.FontSize.MEDIUM,
                LatticePrinter.FontStyle.BOLD);
        mPrinter.printText("Have a nice day!\n\n\n\n\n\n\n\n", LatticePrinter.FontFamily.SONG, LatticePrinter.FontSize.MEDIUM,
                LatticePrinter.FontStyle.BOLD);

        mPrinter.submitPrint();
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        // 取 drawable 的长宽
        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();

        // 取 drawable 的颜色格式
        Bitmap.Config config = drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                : Bitmap.Config.RGB_565;
        // 建立对应 bitmap
        Bitmap bitmap = Bitmap.createBitmap(w, h, config);
        // 建立对应 bitmap 的画布
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, w, h);
        // 把 drawable 内容画到画布中
        drawable.draw(canvas);
        return bitmap;
    }

    // Bitmap → byte[]
    public static byte[] bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }
}
