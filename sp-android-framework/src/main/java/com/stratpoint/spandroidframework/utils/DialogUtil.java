package com.stratpoint.spandroidframework.utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.stratpoint.spandroidframework.activity.BaseActionBarActivity;
import com.stratpoint.spandroidframework.view.SPTextView;
import com.stratpoint.spandroidframework.R;


public class DialogUtil {

    public static void showOKCancel(Context context, String title, String message, String positiveTxt, DialogOnClickListener positiveDListener, String negativeTxt, DialogOnClickListener negativeDListener) {
        SimpleDialog dialog = SimpleDialog.newInstance();
        dialog.initDialog(context, title, message, positiveTxt, positiveDListener, negativeTxt, negativeDListener, Color.BLACK);
        FragmentTransaction ft = null;
        if (context instanceof BaseActionBarActivity) {
            ft = ((BaseActionBarActivity) context).getFragmentManager().beginTransaction();
        }
        ft.add(dialog, null);
        ft.commitAllowingStateLoss();
    }


    public static void showOK(Context context, String title, String message, String positiveTxt, DialogOnClickListener positiveDListener) {
        SimpleDialog dialog = SimpleDialog.newInstance();
        dialog.initDialog(context, title, message, positiveTxt, positiveDListener, null, null, Color.BLACK);
        FragmentTransaction ft = null;
        if (context instanceof BaseActionBarActivity) {
            ft = ((BaseActionBarActivity) context).getFragmentManager().beginTransaction();
        }

        ft.add(dialog, null);
        ft.commitAllowingStateLoss();
    }

    public static void showColoredOK(Context context, String title, String message, String positiveTxt, DialogOnClickListener positiveDListener) {
        SimpleDialog dialog = SimpleDialog.newInstance();
        if (Build.VERSION.SDK_INT > 22) {
            dialog.initDialog(context, title, message, positiveTxt, positiveDListener, null, null, context.getResources().getColor(R.color.text_validation, null));
        } else {
            dialog.initDialog(context, title, message, positiveTxt, positiveDListener, null, null, context.getResources().getColor(R.color.text_validation));
        }
        FragmentTransaction ft = null;
        if (context instanceof BaseActionBarActivity) {
            if (!((BaseActionBarActivity) context).isFinishing()) {
                ft = ((BaseActionBarActivity) context).getFragmentManager().beginTransaction();
            } else {
                return;
            }
        }
        ft.add(dialog, null);
        ft.commitAllowingStateLoss();
    }

    public static void showColoredOKCancel(Context context, String title, String message, String positiveTxt, DialogOnClickListener positiveDListener, String negativeTxt, DialogOnClickListener negativeDListener) {
        SimpleDialog dialog = SimpleDialog.newInstance();
        if (Build.VERSION.SDK_INT > 22) {
            dialog.initDialog(context, title, message, positiveTxt, positiveDListener, negativeTxt, negativeDListener, context.getResources().getColor(R.color.text_validation, null));
        } else {
            dialog.initDialog(context, title, message, positiveTxt, positiveDListener, negativeTxt, negativeDListener, context.getResources().getColor(R.color.text_validation));
        }

        FragmentTransaction ft = null;
        if (context instanceof BaseActionBarActivity) {
            ft = ((BaseActionBarActivity) context).getFragmentManager().beginTransaction();
        }
        ft.add(dialog, null);
        ft.commitAllowingStateLoss();
    }


    @SuppressLint("ValidFragment")
    public static class SimpleDialog extends DialogFragment implements View.OnClickListener {
        private Context context;
        private String title;
        private String message;
        private String positiveTxt;
        private DialogOnClickListener positiveDListener;
        private String negativeTxt;
        private DialogOnClickListener negativeDListener;
        private int messageColor;

        private Button positiveBtn;
        private Button negativeBtn;


        public SimpleDialog() {
        }

        public static SimpleDialog newInstance() {
            Bundle args = new Bundle();
            SimpleDialog fragment = new SimpleDialog();
            fragment.setArguments(args);
            return fragment;
        }

        public void initDialog(Context context, String title, String message, String positiveTxt, DialogOnClickListener positiveDListener, String negativeTxt, DialogOnClickListener negativeDListener, int messageColor) {
            this.context = context;
            this.title = title;
            this.message = message;
            this.positiveTxt = positiveTxt;
            this.positiveDListener = positiveDListener;
            this.negativeTxt = negativeTxt;
            this.negativeDListener = negativeDListener;
            this.messageColor = messageColor;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            setRetainInstance(true);
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View alertDialogLayout = inflater.inflate(R.layout.dialog_cancel_ok, null);
            ((SPTextView) alertDialogLayout.findViewById(R.id.dialog_title)).setText(title);

            SPTextView messageTextView = (SPTextView) alertDialogLayout.findViewById(R.id.dialog_message);


            messageTextView.setText(message);
            messageTextView.setTextColor(messageColor);


            positiveBtn = (Button) alertDialogLayout.findViewById(R.id.btn_positive);
            positiveBtn.setOnClickListener(this);

            negativeBtn = (Button) alertDialogLayout.findViewById(R.id.btn_negative);
            negativeBtn.setOnClickListener(this);

            positiveBtn.setText(positiveTxt);
            if (!ValidationUtils.isEmpty(negativeTxt)) {
                negativeBtn.setText(negativeTxt);
            } else {
                negativeBtn.setVisibility(View.GONE);
            }

            alert.setView(alertDialogLayout);
            alert.setCancelable(false);


//            alert.setPositiveButton(positiveTxt, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    if (null != positiveDListener) {
//                        positiveDListener.onClick();
//                    }
//                    dialog.dismiss();
//                }
//            });
//
//            if (null != negativeTxt) {
//                alert.setNegativeButton(negativeTxt, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        if (null != negativeDListener) {
//                            negativeDListener.onClick();
//                        }
//                        dialog.dismiss();
//                    }
//                });
//            }

            setCancelable(false);
            return alert.create();
        }

        @Override
        public void onStart() {
            super.onStart();

            Typeface normalFont = Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.fontface_path_normal));


            Button positiveBtn = ((AlertDialog) getDialog()).getButton(DialogInterface.BUTTON_POSITIVE);
            Button negativeBtn = ((AlertDialog) getDialog()).getButton(DialogInterface.BUTTON_NEGATIVE);

            if(null != normalFont ){
                positiveBtn.setTypeface(normalFont);
                negativeBtn.setTypeface(normalFont);
            }
        }


        @Override
        public void onClick(View v) {
            if (v == positiveBtn && null != positiveDListener) {
                positiveDListener.onClick();
            } else if (v == negativeBtn && null != negativeDListener) {
                negativeDListener.onClick();
            }
            dismiss();
        }
    }

    public interface DialogOnClickListener {
        public void onClick();
    }
}
