package com.stratpoint.spandroidframework.utils;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by angelaalmoro on 9/18/15.
 */
public class ValidationUtils {


    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");

    public static final Pattern VALID_NUMBER_REGEX =
            Pattern.compile("^\\d+(\\.\\d{1,2})?$");

    public static final Pattern VALID_EDITTEXT_REGEX =
            Pattern.compile("^[a-zA-Z0-9_!@#$%&():;,'*+/=?`\"{|}~^\n. -]+$");
    /**
     * check if string is empty
     *return true if null or length is 0
     * @param str
     */
    public static boolean isEmpty(String str) {
        if (null == str) {
            return true;
        }

        if (0 == str.trim().length()) {
            return true;
        }

        return false;
    }


    /**
     * check if wrong email format
     */
    public static boolean isValidEmail(String email) {
        Pattern pattern = VALID_EMAIL_ADDRESS_REGEX;
        Matcher matcher = pattern.matcher(email);

        if (matcher.matches() == true) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidEditText(String text) {
        Pattern pattern = VALID_EDITTEXT_REGEX;
        Matcher matcher = pattern.matcher(text);
        if (matcher.matches() == true) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidNumber(String number) {
        Pattern pattern = VALID_NUMBER_REGEX;
        Matcher matcher = pattern.matcher(number);
        if (matcher.matches() == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * check if valid old password
     */
    public static boolean isValidOldPassword(String password) {
        if (password.trim().length() < 8) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * validate a date format yyyyMMdd is less than today's date
     * returns true if param date is less than current date
     */
    public static boolean isValidBirthday(int date) {
        return date <= Integer.parseInt(DateUtils.formatDateToString(new Date(), DateUtils.yyyyMMdd));
    }


}
