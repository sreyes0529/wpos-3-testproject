package com.stratpoint.spandroidframework.utils;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Created by adelumban on 4/21/16.
 */
public class Base64Utils {

    public static String decryptUrl(String url) throws UnsupportedEncodingException {
        String encrypted = url.substring(url.indexOf(":") + 1);
        return new String(Base64.decode(encrypted, Base64.DEFAULT), "UTF-8");
    }


    public static String decryptString(String encrypted) {
        String decryptedString;

        try {
            decryptedString = new String(Base64.decode(encrypted, Base64.DEFAULT), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            decryptedString = "";
        }

        return decryptedString;
    }

    /**
     * String to encrypt to base64
     * */
    public static String encrypt(byte[] toBase64){
        String base64String;

        try {
            base64String = new String(Base64.encode(toBase64, Base64.DEFAULT), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            base64String = "";
        }

        return base64String;
    }
}
