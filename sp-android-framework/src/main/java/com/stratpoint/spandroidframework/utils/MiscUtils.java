package com.stratpoint.spandroidframework.utils;

import android.content.Context;
import android.util.TypedValue;

/**
 * Created by jesper on 4/28/16.
 */
public class MiscUtils {
    public static int getPixelFromDp(Context context, int dp){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static int getPixelFromSp(Context context, int sp){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }
}
