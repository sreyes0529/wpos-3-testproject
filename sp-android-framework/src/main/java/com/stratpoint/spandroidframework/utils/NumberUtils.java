package com.stratpoint.spandroidframework.utils;

import java.math.BigDecimal;

/**
 * Created by adelumban on 5/4/16.
 */
public class NumberUtils {

    /**
     * @param value float value to be formatted in ##.##
     **/
    public static String formatDecimal(float value){
        return String.format("%.2f",value);
    }

    /**
     * @param value float value to be formatted in ##.##
     **/
    public static String formatDecimal(double value){
        return String.format("%.2f",value);
    }


    /**
     * @param value float value to be formatted in ##.##
     **/
    public static String formatDecimal(BigDecimal value){
        return String.format("%.2f",value);
    }
}
