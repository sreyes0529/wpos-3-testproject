package com.stratpoint.spandroidframework.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateUtils {

    /**
     * formats the date to Jan
     **/
    public static final String MMM = "MMM";

    /**
     * formats the date to January
     **/
    public static final String MMMM = "MMMM";

    /**
     * formats the date to 21 Jan
     **/
    public static final String dd_MMM = "dd MMM";

    /**
     * formats the date to Jan 2015
     **/
    public static final String MMM_yyyy = "MMM yyyy";

    /**
     * formats the date to January 2015
     **/
    public static final String MMMM_yyyy = "MMMM yyyy";

    /**
     * formats the date to January 21
     **/
    public static final String MMMM_dd = "MMMM dd";

    /**
     * formats the date to Jan 21
     **/
    public static final String MMM_dd = "MMM dd";

    /**
     * formats the date to January 21, 01:00 PM
     **/
    public static final String MMMM_dd_hh_mm_a = "MMMM dd, hh:mm a";

    /**
     * formats the date to 12/30/15 01:00PM
     **/
    public static final String MM_dd_yy_hh_mm_a = "MM/dd/yy hh:mma";

    /**
     * format the date to January 01, 2015
     */
    public static String MMMM_dd_yyyy = "MMMM dd, yyyy";

    /**
     * format the date to January 01, 2015 Thu
     */
    public static String MMMM_dd_yyyy_E = "MMMM dd, yyyy E";

    /**
     * format the date to Jan 01, 2015
     */
    public static String MMM_dd_yyyy = "MMM dd, yyyy";

    /**
     * format the date to Jan 01, 2015 Thu
     */
    public static String MMM_dd_yyyy_E = "MMM dd, yyyy E";


    /**
     * format the date to 20150130
     */
    public static String yyyyMMdd = "yyyyMMdd";

    /**
     * format the date to 01/30/15
     */
    public static String MMddyyyy = "MM/dd/yyyy";

    /**
     * format the date to 2015-01-30
     */
    public static String yyyy_MM_dd = "yyyy-MM-dd";

    /**
     * format the date to 2015-01
     */
    public static String yyyy_MM = "yyyy-MM";

    /**
     * format the date to 02:52 PM, Jan 01
     */
    public static String hh_mm_a_MM_dd = "hh:mm a, MMM dd";

    /**
     * format the date to 2015-01-01 17:17:28
     */
    public static String yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";

    /**
     * format the time to 17:17:28
     */
    public static String HH_mm_ss = "HH:mm:ss";

    /**
     * format String yyyyMMdd to January 01, 2015
     **/
    public static String formatStringDate(String stringDate) {
        if (stringDate.length() > 0) {
            return formatStringDate(stringDate, MMMM_dd_yyyy);
        } else {
            return "";
        }
    }

    /**
     * format String yyyyMMdd to DateUtil.format
     **/
    public static String formatStringDate(String stringDate, String dateFormat) {
        if (stringDate != null && !stringDate.equals("")) {

            SimpleDateFormat format = new SimpleDateFormat(dateFormat);
            String newStringDate = format.format(formatStringToDate(stringDate));

            return newStringDate;
        } else {
            return "";
        }
    }


    /**
     * format calendar to DateUtil.format
     **/
    public static String formatCalendar(Calendar cal, String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        return format.format(cal.getTime());
    }

    /**
     * format StringDate from and to the assigned date format
     **/
    public static String formatStringDate(String stringDate, String dateFormatFrom, String dateFormatTo) {
        String newStringDate = "";
        try {
            Date date = new SimpleDateFormat(dateFormatFrom).parse(stringDate);
            SimpleDateFormat format = new SimpleDateFormat(dateFormatTo);
            newStringDate = format.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newStringDate;
    }

    /**
     * format Date to String
     *
     * @param date
     * @param dateFormat use DateUtils.<format>
     **/
    public static String formatDateToString(Date date, String dateFormat) {

        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        String stringDate = format.format(date);

        return stringDate;
    }

    /**
     * format Time to String
     *
     * @param date
     * @param dateFormat use DateUtils.<format>
     **/
    public static String formatTimeToString(Date date, String dateFormat) {

        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        String stringDate = format.format(date);

        return stringDate;
    }

    /**
     * format String to Date from yyyyxMMxdd to Date where x is delimeter.
     *
     * @param delimiter - (e.g. -, _ , /)
     **/
    public static Date formatStringToDate(String stringDate, String delimiter) {

        String[] splitedDate = stringDate.split(delimiter);


        int y = Integer.parseInt(splitedDate[0]);
        int m = Integer.parseInt(splitedDate[1]) - 1;
        int d = Integer.parseInt(splitedDate[2]);

        GregorianCalendar cal = new GregorianCalendar(y, m, d);
        return cal.getTime();
    }

    /**
     * format String to Date from yyyyMMdd to Date
     **/
    public static Date formatStringToDate(String stringDate) {

        int y = Integer.parseInt(stringDate.substring(0, 4));
        int m = Integer.parseInt(stringDate.substring(4, 6)) - 1;
        int d = Integer.parseInt(stringDate.substring(6, 8));

        GregorianCalendar cal = new GregorianCalendar(y, m, d);
        return cal.getTime();
    }

    /**
     * returns x format not xx format
     */
    public static int getCurrentMonth() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.MONTH);
    }

    /**
     * returns x format not xx format
     */
    public static int getCurrentDayOfMonth() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public static int getCurrentYear() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.YEAR);
    }

    public static int getCurrentHour() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.HOUR_OF_DAY);
    }

    public static int getCurrentMinute() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.MINUTE);
    }

    /**
     * convert 24hr format to 12hr format
     * params: hourOfDay, minute
     */
    public static String buildTimeString(int hourOfDay, int minute) {
        String time = "";
        if (hourOfDay > 12) {
            time = String.valueOf(hourOfDay) + ":";
            if (minute < 10) {
                time += "0" + (String.valueOf(minute)) + ":" + "00";
            } else {
                time += (String.valueOf(minute)) + ":" + "00";
            }
        }
        if (hourOfDay == 12) {
            time = "12" + ":";
            if (minute < 10) {
                time += "0" + (String.valueOf(minute)) + ":" + "00";
            } else {
                time += (String.valueOf(minute)) + ":" + "00";
            }
        }
        if (hourOfDay < 12) {
            time = String.valueOf(hourOfDay) + ":";
            if (minute < 10) {
                time += "0" + (String.valueOf(minute)) + ":" + "00";
            } else {
                time += (String.valueOf(minute)) + ":" + "00";
            }
        }
        if (hourOfDay < 10) {
            time = "0" + String.valueOf(hourOfDay) + ":";
            if (minute < 10) {
                time += "0" + (String.valueOf(minute)) + ":" + "00";
            } else {
                time += (String.valueOf(minute)) + ":" + "00";
            }
        }

        return time;
    }

    /**
     * convert date to mm/dd/yyyy string format
     * params: year, month, day
     */
    public static String buildSlashDateString(int year, int monthOfYear, int dayOfMonth) {
        String date = "";

        date = monthOfYear + 1 + "/" + dayOfMonth + "/" + year;

        return date;
    }

    /**
     * convert date to yyyy-mm-dd string format
     * params: year, month, day
     */
    public static String buildDashDateString(int year, int monthOfYear, int dayOfMonth) {
        String date = "";

        date = year + "-" + String.valueOf(monthOfYear + 1) + "-" + dayOfMonth;

        return date;
    }

    /**
     * convert date to yyyyMMdd string format
     *
     * @param year
     * @param month
     * @param day
     */
    public static String buildDateString(int year, int monthOfYear, int dayOfMonth) {
        String date = String.valueOf(year);

        if ((monthOfYear + 1) < 10) {
            date += "0" + String.valueOf(monthOfYear + 1);
        } else {
            date += String.valueOf(monthOfYear + 1);
        }

        if ((dayOfMonth) < 10) {
            date += "0" + dayOfMonth;
        } else {
            date += dayOfMonth;
        }

        return date;
    }

    /**
     * convert date to yyyy-MM-dd string format
     *
     * @param year
     * @param month
     * @param day
     */
    public static String buildDashDateString2(int year, int monthOfYear, int dayOfMonth) {
        String date = String.valueOf(year);

        if ((monthOfYear + 1) < 10) {
            date += "-0" + String.valueOf(monthOfYear + 1);
        } else {
            date += "-" + String.valueOf(monthOfYear + 1);
        }

        if ((dayOfMonth) < 10) {
            date += "-0" + dayOfMonth;
        } else {
            date += "-" + dayOfMonth;
        }

        return date;
    }


    /**
     * <p>get the date of a calenday by day of the week</p>
     * <p/>
     * Calendar.SUNDAY
     * Calendar.MONDAY
     * Calendar.TUESDAY
     * Calendar.WEDNESDAY
     * Calendar.THURSDAY
     * Calendar.FRIDAY
     * Calendar.SATURDAY
     **/
    public static String getDatebyDay(Calendar calendar, int dayOfWeek, String format) {

        Calendar datebyDayCal = Calendar.getInstance();
        datebyDayCal.setTime(calendar.getTime());
        datebyDayCal.set(Calendar.DAY_OF_WEEK, dayOfWeek);

        return formatCalendar(datebyDayCal, format);
    }

    public static String getCurrentDate(String format) {
        Calendar datebyDayCal = Calendar.getInstance();
        return formatCalendar(datebyDayCal, format);
    }

    /**
     * @return current calendar instance
     */
    public static Calendar getCurrentCal() {
        Calendar cal = Calendar.getInstance();
        return cal;
    }

    /**
     * @param date1 - yyyyMMdd date format.
     * @param date2 - yyyyMMdd date format.
     * @return days difference of date1 and date2;
     */
    public static int getDaysDiff(String date1, String date2) {
        return Integer.parseInt(date1) - Integer.parseInt(date2);
    }

    /**
     * @param timeInMillis
     * @return eg: If current day
     * 'hh:mm a' -> '9:04 PM'
     * If previous day
     * 'Yesterday'
     * Other
     * 'MMMM dd, yyyy' -> 'June 21, 2016'
     */
    public static String getMsgDateTime(long timeInMillis) {
        Calendar cal = Calendar.getInstance();

        long diff = cal.getTimeInMillis() - timeInMillis;
        if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) > 0 &&
                TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) < 2) {
            return "Yesterday";
        } else if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) < 1) {
            cal.setTimeInMillis(timeInMillis);
            return new SimpleDateFormat("hh:mm a").format(cal.getTime());
        } else {
            cal.setTimeInMillis(timeInMillis);
            return new SimpleDateFormat("MMMM dd, yyyy").format(cal.getTime());
        }
    }

    /**
     * Uses "MMMM dd, yyyy hh:mm a" as default format
     * @param timeInMillis
     * @return
     */
    public static String getDateTime(long timeInMillis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeInMillis);
        return new SimpleDateFormat("MMMM dd, yyyy hh:mm a").format(cal.getTime());
    }

    /**
     * Uses "MMMM dd, yyyy" as default format
     * @param timeInMillis
     * @return
     */
    public static String getDate(long timeInMillis) {
        String format = "MMMM dd, yyyy";
        return getDate(timeInMillis, format);
    }

    public static String getDate(long timeInMillis, String format) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeInMillis);
        return new SimpleDateFormat(format).format(cal.getTime());
    }

    /**
     * @param time   eg: "2016-03-10T03:38:28.000Z"
     * @param format eg: "yyyy-MM-dd'T'hh:mm:ss.SSSZ"
     * @return returns epoch time representation based on the specified string time and format
     */
    public static long getEpochTime(String time, String format) {
        long epochTime;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        try {
            epochTime = simpleDateFormat.parse(time).getTime();
        } catch (ParseException e) {
            epochTime = 0;
            e.printStackTrace();
        }
        return epochTime;
    }
}
