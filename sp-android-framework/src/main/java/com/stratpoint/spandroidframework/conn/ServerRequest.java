package com.stratpoint.spandroidframework.conn;

/**
 * Created by adelumban on 4/29/16.
 */

public interface ServerRequest {
    void onServerRequest(int id);
}