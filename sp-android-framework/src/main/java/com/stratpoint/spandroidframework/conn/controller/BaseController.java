package com.stratpoint.spandroidframework.conn.controller;

import android.content.Context;

import com.google.gson.Gson;
import com.stratpoint.spandroidframework.conn.HttpConnection;


/**
 * Created by adelumban on 1/11/16.
 */
public class BaseController {
    protected HttpConnection mHttpconn;
    protected Context mContext;
//    protected HttpConnection.HttpConnectionSetter connectionSetter;
    protected Gson mGson = new Gson();
    public BaseController(Context context, HttpConnection.HttpConnectionSetter connectionSetter) {
        mContext = context;
        mHttpconn = new HttpConnection();
        connectionSetter.onSetHttpConnection(mHttpconn);
    }

}
