package com.stratpoint.spandroidframework.utils;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;

/**
 * Created by adelumban on 5/13/16.
 */
public class FileUtils {

    private static final String TAG = "FileUtils.class";

    private static final int MEGABYTE = 1024;

    private Context mContext;
    private boolean isCanceled = false;

    public FileUtils(Context context) {
        mContext = context;
    }

    /**
     * don't forget to add permission in your AndroidManifest.xml
     * android.permission.WRITE_EXTERNAL_STORAGE
     * <p/>
     * convert the inputStream to encryptedfile in a directory
     *
     * @param key - string key for encryption
     *
     */
    public void encryptAndSaveFile(String key,ResponseBody responseBody, File directory, DownloadListener dlListener) throws IOException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException {
        // Here you read the cleartext.
        InputStream inputStream = responseBody.byteStream();
        // This stream write the encrypted text. This stream will be wrapped by
        // another stream.
        FileOutputStream fos = new FileOutputStream(directory);

        // Length is 16 byte
        SecretKeySpec sks = new SecretKeySpec(key.getBytes(), "AES");

        // Create cipher
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, sks);

        // Wrap the output stream
        CipherOutputStream cos = new CipherOutputStream(fos, cipher);
        // Write bytes
        int b;
        double total = 0;
        byte[] d = new byte[MEGABYTE];
        while ((b = inputStream.read(d)) != -1 && !isCanceled) {
            total += b;
            dlListener.onDownloading((total / 1024) / 1024, String.valueOf((responseBody.contentLength() / 1024) / 1024));
            cos.write(d, 0, b);
        }
        // Flush and close streams.
        cos.flush();
        cos.close();
        inputStream.close();
        dlListener.onDownloadFinish(isCanceled);
    }

    /**
     *
     * @param key - string to decrypt file
     *
     * */
    public void decryptSavedFile(String key,File encryptedFile, File decryptedFile, DecryptionListner decryptionListner) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        InputStream fis = new FileInputStream(encryptedFile);

        SecretKeySpec sks = new SecretKeySpec(key.getBytes(), "AES");

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);
        CipherInputStream cis = new CipherInputStream(fis, cipher);

        BufferedSink sink = Okio.buffer(Okio.sink(decryptedFile));
        sink.writeAll(Okio.source(cis));
        sink.close();

        decryptionListner.onDecryptFinish();

    }

    /**
     * don't forget to add permission in your AndroidManifest.xml
     * android.permission.WRITE_EXTERNAL_STORAGE
     * <p/>
     * convert the inputStream to file in a directory
     */
    public static void copyToFile(ResponseBody responseBody, File directory) throws IOException {
        BufferedSink sink = Okio.buffer(Okio.sink(directory));
        sink.writeAll(responseBody.source());
        sink.close();
    }


    public void setCanceled(boolean canceled) {
        isCanceled = canceled;
    }


    public interface DownloadListener {
        void onDownloading(double progress, String max);

        void onDownloadFinish(boolean isCanceled);
    }

    public interface  DecryptionListner{
        void onDecryptFinish();
    }
}
