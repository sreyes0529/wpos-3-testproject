package com.stratpoint.spandroidframework.conn.controller;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.stratpoint.spandroidframework.utils.AppConstants;
import com.stratpoint.spandroidframework.utils.FCfg;


/**
 * Created by adelumban on 10/27/15.
 */
public class GeneralPreferenceController implements AppConstants {
    private SharedPreferences generalPreferences;
    private Context mContext;
    private Gson mGson;

    public GeneralPreferenceController(Context context, SharedPreferences generalPreferences) {
        this.generalPreferences = generalPreferences;
        mContext = context;
        mGson = new Gson();
    }

    /**
     * save general prefs here, create your keys in pref.xml
     **/
    public void saveToSharedPref(String key, String value) {
        SharedPreferences.Editor editor = generalPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * get general prefs here using keys created in pref.xml
     **/
    public String getStringValueFromPref(String key) {
        return generalPreferences.getString(key, null);
    }

    public void saveToSharedPref(String key, boolean value) {
        SharedPreferences.Editor editor = generalPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * get boolean value from pref,
     *
     * @return the preference value if it exists, or defValue
     */
    public boolean getbooleanValueFromPref(String key, boolean defValue) {
        return generalPreferences.getBoolean(key, defValue);
    }


    public void saveProtocol(String protocol) {
        saveToSharedPref(PrefKeys.PROTOCOL, protocol);
    }

    public String getProtocol() {
        if (null != getStringValueFromPref(PrefKeys.PROTOCOL)) {
            return getStringValueFromPref(PrefKeys.PROTOCOL);
        } else {
            return FCfg.Networks.SERVER_PROTOCOL;
        }
    }

    public void saveDomain(String ip) {
        saveToSharedPref(PrefKeys.DOMAIN, ip);
    }

    public String getDomain() {
        return getStringValueFromPref(PrefKeys.DOMAIN);
    }

    public boolean contains(String key) {
        return generalPreferences.contains(key);
    }

    public void clear() {
        generalPreferences.edit().clear().commit();
    }

}
