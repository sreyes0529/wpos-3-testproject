package com.stratpoint.stratpointpos.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.stratpoint.spandroidframework.activity.BaseActionBarActivity;
import com.stratpoint.spandroidframework.view.SPButtonView;
import com.stratpoint.spandroidframework.view.SPTextView;
import com.stratpoint.stratpointpos.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by angelaalmoro on 3/27/17.
 */

public class HomeActivity extends BaseActionBarActivity {
    @BindView(R.id.read_card_btn)
    SPButtonView mReadCardBtn;
    @BindView(R.id.print_receipt_btn)
    SPButtonView mPrintReceiptBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

    }



    @OnClick({R.id.read_card_btn, R.id.print_receipt_btn})
    public void onSubmit(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.read_card_btn:
                intent = new Intent(this, ReadCardActivity.class);
                startActivity(intent);
                break;

            case R.id.print_receipt_btn:
                intent = new Intent(this, PrintReceiptActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }
    }

}
