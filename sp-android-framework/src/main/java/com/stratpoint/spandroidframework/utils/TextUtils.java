package com.stratpoint.spandroidframework.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/**
 * Created by adelumban on 8/20/15.
 */
public class TextUtils {
    /**
     * @param activity
     * @param text - text to use on the link
     * @param url - url to use for the text
     * @param linkColor null for default blue color or hex
     * */
    public static SpannableStringBuilder applyLinkOnText(final Activity activity, String text, final String url, final String linkColor) {
        final Typeface fluxBold  = Typeface.createFromAsset(activity.getAssets(), "fonts/flux_bold.otf");

        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        stringBuilder.append(text);
        stringBuilder.setSpan(new ClickableSpan() {

            @Override
            public void onClick(View widget) {
                Intent browse = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                activity.startActivity(browse);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                if (null == linkColor) {
                    ds.setColor(Color.WHITE);
                } else {
                    ds.setColor(Color.parseColor(linkColor));
                }
                ds.setUnderlineText(true);
                ds.setTypeface(fluxBold);
            }


        }, 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return stringBuilder;
    }

    /**
     * @param activity
     * @param text - text to use on the link
     * @param cls - class to start using intent
     * @param linkColor null for default blue color or hex
     * */
    public static SpannableStringBuilder applyIntentOnText(final Activity activity, String text, final Class<?> cls, final String linkColor) {
        final Typeface fluxBold  = Typeface.createFromAsset(activity.getAssets(), "fonts/flux_bold.otf");

        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        stringBuilder.append(text);
        stringBuilder.setSpan(new ClickableSpan() {

            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(activity, cls);
                activity.startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                if (null == linkColor) {
                    ds.setColor(Color.WHITE);
                } else {
                    ds.setColor(Color.parseColor(linkColor));
                }
                ds.setUnderlineText(true);
                ds.setTypeface(fluxBold);
            }


        }, 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return stringBuilder;
    }
}
