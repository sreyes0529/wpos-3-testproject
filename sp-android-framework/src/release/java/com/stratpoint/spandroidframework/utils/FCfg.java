package com.stratpoint.spandroidframework.utils;


/**
 * Created by adelumban on 11/8/16.
 */
public interface FCfg {
    boolean isProdBuild = true;
    interface Networks {
        String SERVER_PROTOCOL = "http://";
        String SERVER_DOMAIN = "prodenvironment.com";


        //TODO: Add connection requirements below for prod. e.g. key headers
    }

}