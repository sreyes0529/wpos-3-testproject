package com.stratpoint.spandroidframework.view;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.stratpoint.spandroidframework.R;


/**
 * Created by adelumban on 11/2/15.
 */
public class ProgressDialog extends DialogFragment implements View.OnClickListener {

    private SPTextView textView;
    private String mMessage = null;
    private boolean isShowing = false;
    private Button cancelBtn;
    private CancelListner mCancelListner;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.progress_dialog_layout, container);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }


    private void init() {
        setRetainInstance(true);
        setCancelable(false);
        if (null == mMessage) {
            mMessage = getString(R.string.loading);
        }
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        textView = (SPTextView) getView().findViewById(R.id.message);
        textView.setText(mMessage);

        cancelBtn = (Button) getView().findViewById(R.id.btn_cancel);
        cancelBtn.setOnClickListener(this);
    }

    public void setMessage(String message) {
        mMessage = message;
        if (null != textView) {
            textView.setText(message);
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (isShowing) {
            return;
        }
        super.show(manager, tag);
        manager.beginTransaction().commitAllowingStateLoss();
        isShowing = true;
    }

    @Override
    public void dismiss() {
        isShowing = false;
        super.dismiss();
    }


    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        isShowing = false;
        super.onDestroyView();
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        isShowing = hidden;
        super.onHiddenChanged(hidden);
    }

    public boolean isShowing() {
        return isShowing;
    }

    public void setIsShowing(boolean isShowing) {
        this.isShowing = isShowing;
    }

    public void setCancelListener(CancelListner cancelListner) {
        cancelBtn.setVisibility(View.VISIBLE);
        mCancelListner = cancelListner;
    }

    @Override
    public void onClick(View v) {
        if (null != mCancelListner) {
            mCancelListner.onCancel();
        }
    }

    public interface CancelListner {
        void onCancel();
    }


}
