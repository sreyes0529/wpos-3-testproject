package com.stratpoint.spandroidframework.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.stratpoint.spandroidframework.R;


/**
 * Created by adelumban on 4/13/16.
 */
public class SPEditText extends EditText {
    public SPEditText(Context context) {
        super(context);
        if (!isInEditMode()) {
            init(null);
        }
    }

    public SPEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init(attrs);
        }
    }

    public SPEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            init(attrs);
        }
    }

    @SuppressLint("NewApi")
    public SPEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if (!isInEditMode()) {
            init(attrs);
        }
    }

    private void init(AttributeSet attrs) {
        setLongClickable(false);
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.fontface);
            String fontpath = a.getString(R.styleable.fontface_fontpath);
            if (fontpath != null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), fontpath);
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }

}
