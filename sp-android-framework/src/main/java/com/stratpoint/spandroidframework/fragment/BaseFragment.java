package com.stratpoint.spandroidframework.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.google.gson.Gson;
import com.stratpoint.spandroidframework.activity.BaseActionBarActivity;
import com.stratpoint.spandroidframework.conn.ErrorResponseCallback;
import com.stratpoint.spandroidframework.conn.HttpConnection;
import com.stratpoint.spandroidframework.conn.ServerRequest;
import com.stratpoint.spandroidframework.conn.controller.GeneralPreferenceController;
import com.stratpoint.spandroidframework.model.BaseResponse;
import com.stratpoint.spandroidframework.utils.AppConstants;
import com.stratpoint.spandroidframework.utils.DialogUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by adelumban on 2/9/16.
 */
public class BaseFragment extends Fragment implements ServerRequest, AppConstants {

    protected Context mContext;
    protected HttpConnection mHttpConnection;
    protected SharedPreferences generalPreferences;
    protected GeneralPreferenceController genPrefController;
    protected int httpRequestID = -1;
    protected Gson mGson = new Gson();
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        generalPreferences = getActivity().getSharedPreferences(PrefKeys.GEN_PREF, Context.MODE_PRIVATE);
        genPrefController = new GeneralPreferenceController(getActivity(), generalPreferences);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = getActivity();
        init();
        populateView();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
    }

    /**
     * called onActivityCreated
     */
    protected void init() {

    }

    /**
     * called after onViewCreated
     */
    protected void populateView() {
    }

    /**
     * show progress dialog on httpConnection
     * use the parameter to determine the view that has been clicked
     * to handle different request
     *
     * @param id
     **/
    public void initHttpConnection(int id) {
        httpRequestID = id;
        initHttpConnection(id, true);
    }

    /**
     * show progress dialog on httpConnection
     * use the parameter to determine the view that has been clicked
     * to handle different request
     *
     * @param id
     * @param hasProgress - set a progress to be visible or not.
     **/
    public void initHttpConnection(int id, boolean hasProgress) {
        httpRequestID = id;
        ((BaseActionBarActivity) getActivity()).setmServerRequest(this);
        ((BaseActionBarActivity) getActivity()).initHttpConnection(id, hasProgress);
    }

    @Override
    public void onServerRequest(int id) {

    }

    public void initHttpConnectionFail() {
        if (null != getActivity()) {
            ((BaseActionBarActivity) getActivity()).initHttpConnectionFail();
        }
    }


    public void initResponseHandler(BaseResponse baseResponse) {
        if (null != getActivity()) {
            ((BaseActionBarActivity) getActivity()).initResponseHandler(baseResponse);
        }
    }

    public void refreshLastRequest() {
        initHttpConnection(httpRequestID);
    }

    public void refresh() {

    }

    public void endProgress() {
        if (null != getActivity()) {
            ((BaseActionBarActivity) getActivity()).endProgress();
        }
    }

    public void onClickTitle() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    public ErrorResponseCallback.OnErrorResponse onErrorResponse() {
        return ((BaseActionBarActivity) getActivity());
    }

    public HttpConnection.HttpConnectionSetter connectionSetter() {
        return ((BaseActionBarActivity) getActivity());
    }

    public void setProgressDialogMessage(final String message) {
        ((BaseActionBarActivity) getActivity()).setProgressDialogMessage(message);
    }

    public void showOKDialog(final Context context, final String title, final String message, final String positiveTxt, final DialogUtil.DialogOnClickListener positiveDListener) {
        ((BaseActionBarActivity) getActivity()).showOKDialog(context, title, message, positiveTxt, positiveDListener);
    }

    public void showValidationOKDialog(final Context context, final String title, final String message, final String positiveTxt, final DialogUtil.DialogOnClickListener positiveDListener) {
        ((BaseActionBarActivity) getActivity()).showValidationOKDialog(context, title, message, positiveTxt, positiveDListener);
    }

    public void showValidationOKCancelDialog(final Context context, final String title, final String message, final String positiveTxt, final DialogUtil.DialogOnClickListener positiveDListener, final String negativeTxt, final DialogUtil.DialogOnClickListener negativeDListener) {
        ((BaseActionBarActivity) getActivity()).showValidationOKCancelDialog(context, title, message, positiveTxt, positiveDListener, negativeTxt, negativeDListener);
    }


    public void showOKCancelDialog(final Context context, final String title, final String message, final String positiveTxt, final DialogUtil.DialogOnClickListener positiveDListener, final String negativeTxt, final DialogUtil.DialogOnClickListener negativeDListener) {
        ((BaseActionBarActivity) getActivity()).showOKCancelDialog(context, title, message, positiveTxt, positiveDListener, negativeTxt, negativeDListener);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
    }

    protected boolean hasInternetConnection() {
        return ((BaseActionBarActivity) getActivity()).hasInternetConnection();
    }


}
