package com.stratpoint.spandroidframework.conn;

import com.stratpoint.spandroidframework.model.BaseResponse;

public class ErrorResponseCallback {
    public interface OnErrorResponse {
        void onError(BaseResponse baseResponse);
    }
}
